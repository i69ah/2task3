<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/style/index.css">
    <title>Update</title>
</head>
<body>
<div class="wrapper">
    <form action="/save.php?id=<?= $article->id ?>" method="post">
        <input type="text" name="title" placeholder="<?= $article->title ?>" value="<?= $article->title ?>">
        <textarea
                name="content"
                id="" cols="170"
                rows="8"
                placeholder="<?= $article->content ?>"
        ><?= $article->content ?></textarea>
        <button type="submit">Добавить</button>
    </form>
</div>
</body>
</html>