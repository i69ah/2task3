<?php
/** var \App\View $this */
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/style/index.css">
    <title>Admin Panel</title>
</head>
<body>
<div class="wrapper">
    <?php
    foreach ($this->articles as $article) {
        ?>
        <div class="article">
            <div class="article__body">
                <div class="article__header">
                    <div class="article__title">
                        <?= $article->title ?>
                    </div>
                    <div class="article__author">
                        <?= $article->author->name ?>
                    </div>
                </div>
                <div class="hr"></div>
                <div class="article__content">
                    <?= $article->content ?>
                </div>
            </div>
            <div class="admin">
                <div class="btn__list">
                    <a href="/update.php?id=<?= $article->id ?>">
                        <button>Изменить</button>
                    </a>
                    <a href="/delete.php?id=<?= $article->id ?>">
                        <button>Удалить</button>
                    </a>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
    <form action="/create.php" method="post">
        <input type="text" name="title" placeholder="Enter the title">
        <textarea name="content" id="" cols="170" rows="8" placeholder="Enter the content of new article"></textarea>
        <button type="submit">Добавить</button>
    </form>
</div>
</body>
</html>