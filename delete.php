<?php

if (!isset($_GET['id'])) {
    header('location: http://2task2/index.php');
}

$id = (int)$_GET['id'];

if (empty($id)) {
    header('location: http://2task2/index.php');
}

require_once __DIR__ . '/autoload.php';

$article = App\Models\Article::findById($id);

$article->delete();

header('location: http://2task2/index.php');