<?php


if (
    !isset($_POST['title']) || !isset($_POST['content']) ||
    empty($_POST['title']) || empty($_POST['content'])
) {
    header('location: http://2task2/index.php');
}

require_once __DIR__ . '/autoload.php';

$title = htmlspecialchars($_POST['title']);
$content = htmlspecialchars($_POST['content']);

$article = new \App\Models\Article;
$article->title = $title;
$article->content = $content;

$article->save();

header('location: http://2task2/index.php');