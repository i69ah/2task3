<?php


namespace App;

/**
 * Class Db
 * @package App
 */
class Db
{
    private \PDO $dbh;
    private \App\Config $config;


    /**
     * Db constructor.
     */
    public function __construct()
    {
        try {
            $this->config = Config::getInstance();
            $this->dbh = new \PDO(
                'mysql:host=' . $this->config->data['db']['host'] .
                ';dbname=' . $this->config->data['db']['dbname'],
                $this->config->data['user']['login'],
                $this->config->data['user']['password']
            );
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Executes SQL command
     * @param string $sql
     * @param array $params
     * @return bool
     * Returns true in success, false in case of failure
     */
    public function execute(string $sql, array $params=[]): bool
    {
        return $this->dbh->prepare($sql)->execute($params);
    }

    /**
     * Gets the result of SQL query
     * @param string $sql
     * @param string $class
     * @param array $params
     * @return array|false
     * Returns either false if executes with failure or array of data
     */
    public function query(string $sql, string $class, array $params=[])
    {
        $sth = $this->dbh->prepare($sql);
        if (!$sth->execute($params)) {
            return false;
        }
        return $sth->fetchAll(\PDO::FETCH_CLASS, $class);
    }

    /**
     * Gets last inserted object id
     * @return string
     * Returns string type id
     */
    public function getLastId(): string
    {
        return $this->dbh->lastInsertId();
    }
}