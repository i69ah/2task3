<?php


namespace App;


trait GetSetTrait
{
    protected array $data = [];

    /**
     * Gets data by non-existent key of object
     * @param string $key
     * @return mixed|null
     * Returns data element by key $key
     */
    public function __get(string $key)
    {
        if (isset($this->data[$key])) {
            return $this->data[$key];
        }
        return null;
    }

    /**
     * Sets data ($value) by non-existent key ($key) of object
     * @param $key
     * @param $value
     */
    public function __set($key, $value)
    {
        $this->data[$key] = $value;
    }

    /**
     * Checks existence of non-existent object key
     * @param $key
     * @return bool
     * Returns true if data[$key] exists otherwise false
     */
    public function __isset($key)
    {
        return isset($this->data[$key]);
    }
}