<?php
namespace App;

/**
 * Singleton Class Config
 * @package App
 */
class Config
{
    public array $data;
    private static self $instance;

    /**
     * Config constructor.
     * @throws \Exception
     * Throws Exception in case of incorrect config file
     */
    private function __construct()
    {
        $data = include __DIR__ . '/../config.php';
        if (
            empty($data) || !isset($data['db']) || !isset($data['user']) ||
            !isset($data['db']['host']) || !isset($data['db']['dbname']) ||
            !isset($data['user']['login']) || !isset($data['user']['password']) ||
            empty($data['db']['host']) || empty($data['db']['dbname']) ||
            empty($data['user']['login']) || empty($data['user']['password'])
        ) {
            throw new \Exception('Incorrect config file');
        }
        $this->data = $data;
    }

    /**
     * Gets the only one instance of this class
     * @return self
     * Returns the only one instance of Config
     */
    public static function getInstance(): self
    {
        if (empty(self::$instance)) {
            self::$instance = new self;
        }
        return self::$instance;
    }
}