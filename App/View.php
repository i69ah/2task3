<?php


namespace App;


class View implements \Countable, \Iterator
{

    /**
     * Magic methods __get and __set
     */
    use GetSetTrait;

    private int $position = 0;

    /**
     * Sets template with params in var and returns it
     * @param string $template
     * @return string
     * Returns template
     */
    public function render(string $template): string
    {
        ob_start();
        include $template;
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    /**
     * Displays template
     * @param string $template
     */
    public function display(string $template): void
    {
        echo $this->render($template);
    }

    /**
     * Implementation of SPL Iterator and SPL Counter Interfaces
     */
    use IteratorAndCounterTrait;
}