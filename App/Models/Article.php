<?php


namespace App\Models;

use App\Db;

/**
 * Class Article
 * @package App\Models
 */
class Article extends Model
{
    public const TABLE = 'news';
    public string $title;
    public string $content;
    public string $authorId;

    /**
     * If asked property of object is author returns Author object by object id
     * @param string $key
     * @return int|mixed
     */
    public function __get(string $key)
    {
        if ($key === 'author' && !empty($this->authorId)) {
            return Author::findById($this->authorId);
        }
        return 42;
    }


    /**
     * Inserts this object to DB
     * @return bool
     * Returns true if success, false in case of failure
     */
    private function insert(): bool
    {
        $dbh = new Db();

        $fields = get_object_vars($this);

        $cols = [];
        $data = [];

        foreach ($fields as $field => $value) {
            if ('id' === $field) {
                continue;
            }
            $cols[] = $field;
            $data[':' . $field] = $value;
        }

        $sql = 'INSERT INTO ' . self::TABLE .
            ' (' . implode(', ', $cols) . ') 
            VALUES 
            (' . implode(', ', array_keys($data)) . ');';

        $res = $dbh->execute($sql, $data);

        if (!$res) {
            return false;
        }

        $this->id = $dbh->getLastId();

        return true;
    }


    /**
     * Updates DB with this object
     * @return bool
     * Returns true if success, false in case of failure
     */
    private function update(): bool
    {
        $dbh = new Db();

        $fields = get_object_vars($this);

        $data = [];

        foreach ($fields as $field => $value) {
            $data[':' . $field] = $value;
        }

        $sql = 'UPDATE ' . self::TABLE .
            ' SET title=:title, content=:content
              WHERE id=:id;';

        return $dbh->execute($sql, $data);
    }

    /**
     * Trait of save public function
     * Save function either inserts or updates this object in DB according to its id
     */
    use \App\Models\SaveTrait;


    /**
     * Deletes this object from DB
     * @return bool
     * Returns true if success, false in case of failure
     */
    public function delete(): bool
    {
        $dbh = new Db();

        $sql = 'DELETE FROM ' . static::TABLE . ' WHERE id=:id';

        return $dbh->execute($sql, [
            ':id' => $this->id
        ]);
    }
}