<?php

namespace App\Models;

/**
 * Trait SaveTrait
 * @package App\Models
 */
trait SaveTrait
{
    /**
     * Inserts new object in DB or updates changed object in DB
     * @return bool
     * Returns true in success, false in case of failure
     */
    public function save(): bool
    {
        if (empty($this->id)) {
            return $this->insert();
        }
        return $this->update();
    }
}