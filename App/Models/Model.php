<?php

namespace App\Models;

use App\Db;

abstract class Model
{
    protected const TABLE = '';
    public int $id;

    /**
     * Gets all data from selected TABLE of DB
     * @return array
     * Returns array of data
     */
    static function findAll(): array
    {
        $dbh = new Db;
        $sql = 'SELECT * FROM ' . static::TABLE . ';';
        return $dbh->query($sql, static::class);
    }


    /**
     * Gets the DB row by index and returns it
     * @param int $id
     * @return mixed
     * Returns Model object
     */
    static function findById(int $id): Model
    {
        $dbh = new Db;
        $sql = 'SELECT * FROM ' . static::TABLE . ' WHERE authorId=:id;';
        return $dbh->query($sql, static::class, [':id' => $id])[0];
    }
}