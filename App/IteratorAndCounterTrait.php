<?php


namespace App;


trait IteratorAndCounterTrait
{
    public function count(): int
    {
        return count($this->data);
    }

    public function current()
    {
        $keys = array_keys($this->data);
        return $this->data[$keys[$this->position]];
    }

    public function next()
    {
        $this->position++;
    }

    public function key()
    {
        return $this->position;
    }

    public function valid(): bool
    {
        $keys = array_keys($this->data);
        return isset($this->data[$keys[$this->position]]);
    }

    public function rewind()
    {
        $this->position = 0;
    }
}