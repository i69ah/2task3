<?php

if (
    !isset($_POST['title']) || !isset($_POST['content']) || !isset($_GET['id']) ||
    empty($_POST['title']) || empty($_POST['content']) || empty($_GET['id'])
) {
    header('location: http://2task2/index.php');
}

require_once __DIR__ . '/autoload.php';

$id = $_GET['id'];
$title = htmlspecialchars($_POST['title']);
$content = htmlspecialchars($_POST['content']);

$article = App\Models\Article::findById($id);
$article->title = $title;
$article->content = $content;

$article->save();

header('location: http://2task2/index.php');
