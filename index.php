<?php

require_once __DIR__ . '/autoload.php';

$viewIndex = new \App\View();

$viewIndex->articles = \App\Models\Article::findAll();
echo $viewIndex->render(__DIR__ .'/templates/index.php');


//$viewIndex->foo = 'foo';
//$viewIndex->bar = 'barbar';
//foreach ($viewIndex as $prop) {
//    var_dump($prop);
//}